package com.example.lab13_8;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {
    double res;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_second);

        TextView t = findViewById(R.id.list);
        double fv,sv,tv;
        Intent i = getIntent();
        fv = i.getDoubleExtra("f",0);
        sv = i.getDoubleExtra("s",0);
        tv = i.getDoubleExtra("t",0);

        res = (fv + sv + tv) / 3;
        t.setText(String.format("%.2f,\n%.2f,\n%.2f", fv, sv, tv));
    }

    public void onBack(View v) {
        Intent i = new Intent();
        i.putExtra("r", res);
        setResult(RESULT_OK, i);
        finish();
    }

}
