package com.example.lab13_8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText f, s, t;
    TextView res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        f = findViewById(R.id.first);
        s = findViewById(R.id.second);
        t = findViewById(R.id.third);

        res = findViewById(R.id.result);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            double r = data.getDoubleExtra("r",0);
            res.setText(String.format("Ответ: %.2f", r));
        }
    }

    public void onCalculate(View v) {
        double fv = 0;
        double sv = 0;
        double tv = 0;
        try {
            fv = Double.parseDouble(f.getText().toString());
            sv = Double.parseDouble(s.getText().toString());
            tv = Double.parseDouble(t.getText().toString());
        } catch (Exception e) {}

       Intent i = new Intent(this, SecondActivity.class);
       i.putExtra("f",fv);
       i.putExtra("s",sv);
       i.putExtra("t",tv);
       startActivityForResult(i,1);
    }
}